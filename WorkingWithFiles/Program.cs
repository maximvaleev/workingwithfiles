﻿/*Домашнее задание

Создание консольного приложение, записывающее и считывающее информацию в\из файл(а).
Цель:

компилирующееся без ошибок приложение, файлы по заданному пути, консоль со значениями файлов.

Описание/Пошаговая инструкция выполнения домашнего задания:

    Создать директории c:\Otus\TestDir1 и c:\Otus\TestDir2 с помощью класса DirectoryInfo.
    В каждой директории создать несколько файлов File1...File10 с помощью класса File.
    В каждый файл записать его имя в кодировке UTF8. Учесть, что файл может быть удален, либо отсутствовать права на запись.
    Каждый файл дополнить текущей датой (значение DateTime.Now) любыми способами: синхронно и\или асинхронно.
    Прочитать все файлы и вывести на консоль: имя_файла: текст + дополнение.
*/

using System.Text;

namespace WorkingWithFiles;

internal class Program
{
    static async Task Main()
    {
        // Создать директории c:\Otus\TestDir1 и c:\Otus\TestDir2 с помощью класса DirectoryInfo.
        DirectoryInfo[] dirInfos = CreateFolders();
        // В каждой директории создать несколько файлов File1...File10 с помощью класса File.
        CreateFiles(dirInfos);
        // В каждый файл записать его имя в кодировке UTF8.Учесть, что файл может быть удален, либо отсутствовать права на запись.
        List<FileInfo> fileInfos = GetFileInfos(dirInfos);
        WriteNamesToFiles(fileInfos);
        // Каждый файл дополнить текущей датой(значение DateTime.Now) любыми способами: синхронно и\или асинхронно.
        await AppendDateAsync(fileInfos);
        // Прочитать все файлы и вывести на консоль: имя_файла: текст + дополнение.
        ShowFilesContents(fileInfos);
    }

    private static DirectoryInfo[] CreateFolders()
    {
        Console.Write("Создаю папки ... ");
        try
        {
            var dirInfos = new DirectoryInfo[] { new(@"Otus\TestDir1"), new(@"Otus\TestDir2") };
            foreach (DirectoryInfo dirInfo in dirInfos)
            {
                dirInfo.Create();
            }
            Console.WriteLine("готово");
            return dirInfos;
        }
        catch (Exception exc)
        {
            Console.WriteLine($"Ошибка при создании папок: {exc.Message}");
            throw;
        }
    }

    private static void CreateFiles(DirectoryInfo[] dirInfos)
    {
        Console.Write("Создаю файлы ... ");
        foreach (DirectoryInfo dirInfo in dirInfos)
        {
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    File.Create(Path.Combine(dirInfo.FullName, "File" + i + ".txt")).Close();
                }
                catch (Exception exc)
                {
                    Console.WriteLine($"Не удалось создать файл '{"File" + i + ".txt"}' " +
                        $"в папке '{dirInfo.Name}': {exc.Message}");
                    // исключение подавляется
                }
            }
        }
        Console.WriteLine("готово");
    }

    private static List<FileInfo> GetFileInfos(DirectoryInfo[] dirInfos)
    {
        Console.Write("Получаю информацию о файлах ... ");
        List<FileInfo> fileInfos = new();
        foreach (DirectoryInfo dirInfo in dirInfos)
        {
            try
            {
                fileInfos.AddRange(dirInfo.GetFiles());
            }
            catch (Exception exc)
            {
                Console.WriteLine($"Не удалось получить информацию о файлах " +
                    $"папки {dirInfo.Name}: {exc.Message}");
                // исключение подавляется
            }
        }
        Console.WriteLine("готово");
        return fileInfos;
    }

    private static void WriteNamesToFiles(List<FileInfo> fileInfos)
    {
        Console.Write("Записываю в файлы их имена ... ");
        foreach (FileInfo fileInfo in fileInfos)
        {
            try
            {
                // не надо создавать новый файл, если его нет
                fileInfo.Refresh();
                if (fileInfo.Exists)
                {
                    File.WriteAllText(fileInfo.FullName, fileInfo.Name, Encoding.UTF8);
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine($"Не удалось записать текст в файл " +
                    $"{fileInfo.Name}: {exc.Message}");
                // исключение подавляется
            }
        }
        Console.WriteLine("готово");
    }

    private static async Task AppendDateAsync(List<FileInfo> fileInfos)
    {
        Console.Write("Добавляю даты в файлы асинхронно ... ");
        Task[] tasks = new Task[fileInfos.Count];

        // Неверный код
        /*for (int i = 0; i < fileInfos.Count; i++)
        {
            try
            {
                fileInfos[i].Refresh();
                if (fileInfos[i].Exists)
                {
                    tasks[i] = File.AppendAllTextAsync(fileInfos[i].FullName,
                         " " + DateTime.Now.ToShortDateString(),
                         Encoding.UTF8);
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine($"Не удалось дополнить файл " +
                    $"{fileInfos[i].Name} датой: {exc.Message}");
                // исключение подавляется
            }
        }
        await Task.WhenAll(tasks.Where(x => x is not null));
        Console.WriteLine("готово");*/

        // Исправленный код
        for (int i = 0; i < fileInfos.Count; i++)
        {
            fileInfos[i].Refresh();
            if (fileInfos[i].Exists)
            {
                tasks[i] = File.AppendAllTextAsync(fileInfos[i].FullName,
                     " " + DateTime.Now.ToShortDateString(),
                     Encoding.UTF8);
            }
        }
        Task combinedTask = Task.WhenAll(tasks);
        await combinedTask;
        if (combinedTask.Status == TaskStatus.RanToCompletion)
            Console.WriteLine("Даты добавлены во все файлы.");
        else if (combinedTask.Status == TaskStatus.Faulted)
            Console.WriteLine("Не удалось добавить дату в один или несколько файлов");
    }

    private static void ShowFilesContents(List<FileInfo> fileInfos)
    {
        foreach (FileInfo fileInfo in fileInfos)
        {
            try
            {
                Console.WriteLine($"Файл {fileInfo.Name} " +
                    $"содержит: {File.ReadAllText(fileInfo.FullName, Encoding.UTF8)}");
            }
            catch (Exception exc)
            {
                Console.WriteLine($"Не удалось считать содержимое файла " +
                    $"{fileInfo.Name}: {exc.Message}");
                // исключение подавляется
            }
        }
    }
}